from setuptools import setup, find_namespace_packages

setup(name='cleanm folder',
      version='0.0.1',
      description='This package can sorted your files by extension',
      author='Oleksandra Tsyhanok',
      author_email='alekstsy2001@gmail.com',
      license='MIT',
      classifiers=[
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent",
      ],
      packages=find_namespace_packages(),
      entry_points={'console_scripts': ['clean-folder=clean_folder.clean:sort_all']}
      # greeting - команда, яка повинна виконатись у терміналі
      # після '=' пишемо шлях до файлу, де знаходиться функція => hello_world_vvm.main
      # після ':' пишемо назву функції greeting
      )