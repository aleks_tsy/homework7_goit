from pathlib import Path
import shutil
import sys
import re

CYRILLIC_SYMBOLS = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюяєіїґ'
TRANSLATION = ("a", "b", "v", "g", "d", "e", "e", "j", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u",
               "f", "h", "ts", "ch", "sh", "sch", "", "y", "", "e", "yu", "u", "ja", "je", "ji", "g")

TRANS = {}
for c, l in zip(CYRILLIC_SYMBOLS, TRANSLATION):
    TRANS[ord(c)] = l
    TRANS[ord(c.upper())] = l.upper()


def normalize(name: str) -> str:
    t_name = name.translate(TRANS)
    t_name = re.sub(r'\W', '_', t_name)
    return t_name

JPEG_IMAGES = []
JPG_IMAGES = []
PNG_IMAGES = []
SVG_IMAGES = []
MP3_AUDIO = []
ZIP_ARCHIVES = []
AVI_VIDEO = []
MP4_VIDEO = []
MOV_VIDEO = []
MKV_VIDEO = []
DOC_DOCUMENT = []
DOCX_DOCUMENT = []
TXT_DOCUMENT = []
PDF_DOCUMENT = []
XLSX_DOCUMENT = []
PPTX_DOCUMENT = []

OGG_AUDIO = []
WAV_AUDIO = []
AMR_AUDIO = []
MY_OTHER = []

GZ_ARCHIVES = []
TAR_ARCHIVES = []

REGISTER_EXTENSIONS = {
    'JPEG': JPEG_IMAGES,
    'PNG': PNG_IMAGES,
    'JPG': JPG_IMAGES,
    'SVG': SVG_IMAGES,
    'MP3': MP3_AUDIO,
    'ZIP': ZIP_ARCHIVES,
    'AVI': AVI_VIDEO,
    'MP4':MP4_VIDEO,
    'MOV':MOV_VIDEO,
    'MKV':MKV_VIDEO,
    'DOC':DOC_DOCUMENT,
    'DOCX':DOCX_DOCUMENT,
    'TXT':TXT_DOCUMENT,
    'PDF':PDF_DOCUMENT,
    'XLSX':XLSX_DOCUMENT,
    'PPTX':PPTX_DOCUMENT,
    'OGG':OGG_AUDIO,
    'WAV':WAV_AUDIO,
    'AMR':AMR_AUDIO,
    'GZ':GZ_ARCHIVES,
    'TAR':TAR_ARCHIVES,
}

FOLDERS = []
EXTENSIONS = set()
UNKNOWN = set()



def get_extension(filename: str) -> str:
    # перетворюємо розширення файлу на назву папки .jpg -> JPG
    return Path(filename).suffix[1:].upper()



def scan(folder: Path) -> None:
    for item in folder.iterdir():
        # Якщо це папка то додаємо її зі списку FOLDERS і переходимо до наступного елемента папки
        if item.is_dir():
            # перевіряємо, щоб папка не була тією, в яку ми складаємо вже файли.
            if item.name not in ('archives', 'video', 'audio', 'documents', 'images', 'MY_OTHER'):
                FOLDERS.append(item)
                # скануємо цю вкладену папку - рекурсія
                scan(item)
            # перейти до наступного елемента в сканованій папці
            continue

        # Робота з файлом
        ext = get_extension(item.name)  # взяти розширення
        fullname = folder / item.name  # взяти повний шлях до файлу
        if not ext:  # якщо файл не має розширення додати до невідомих
            MY_OTHER.append(fullname)
        else:
            try:
                # взяти список куди покласти повний шлях до файлу
                container = REGISTER_EXTENSIONS[ext]
                EXTENSIONS.add(ext)
                container.append(fullname)
            except KeyError:
                # Якщо ми не реєстрували розширення у REGISTER_EXTENSIONS, то додати до іншого
                UNKNOWN.add(ext)
                MY_OTHER.append(fullname)



def handle_media(filename: Path, target_folder: Path):
    target_folder.mkdir(exist_ok=True, parents=True)
    filename.replace(target_folder / normalize(filename.name))


def handle_video(filename: Path, target_folder: Path):
    target_folder.mkdir(exist_ok=True, parents=True)
    filename.replace(target_folder / normalize(filename.name))


def handle_document(filename: Path, target_folder: Path):
    target_folder.mkdir(exist_ok=True, parents=True)
    filename.replace(target_folder / normalize(filename.name))


def handle_music(filename: Path, target_folder: Path):
    target_folder.mkdir(exist_ok=True, parents=True)
    filename.replace(target_folder / normalize(filename.name))



def handle_other(filename: Path, target_folder: Path):
    target_folder.mkdir(exist_ok=True, parents=True)
    filename.replace(target_folder / normalize(filename.name))


def handle_archive(filename: Path, target_folder: Path):
    # Створюємо папку для архіву
    target_folder.mkdir(exist_ok=True, parents=True)
    # Створюємо папку куди розпакуємо архів
    # Беремо суфікс у файла і удаляємо replace(filename.suffix, '')
    folder_for_file = target_folder / normalize(filename.name.replace(filename.suffix, ''))

    # Створюємо папку для архіву з іменем файлу
    folder_for_file.mkdir(exist_ok=True, parents=True)
    try:
        shutil.unpack_archive(str(filename.resolve()),
                              str(folder_for_file.resolve()))
    except shutil.ReadError:
        print(f'Це не архів {filename}!')
        folder_for_file.rmdir()
        return None
    filename.unlink()


def handle_folder(folder: Path):
    try:
        folder.rmdir()
    except OSError:
        print(f'Помилка видалення папки {folder}')


def main(folder: Path):
    scan(folder)
    for file in JPEG_IMAGES:
        handle_media(file, folder / 'images' / 'JPEG')
    for file in JPG_IMAGES:
        handle_media(file, folder / 'images' / 'JPG')
    for file in PNG_IMAGES:
        handle_media(file, folder / 'images' / 'PNG')
    for file in SVG_IMAGES:
        handle_media(file, folder / 'images' / 'SVG')
    for file in MP3_AUDIO:
        handle_music(file, folder / 'audio' / 'MP3')
    for file in OGG_AUDIO:
        handle_music(file, folder / 'audio' / 'OGG')
    for file in WAV_AUDIO:
        handle_music(file, folder / 'audio' / 'WAV')
    for file in AMR_AUDIO:
        handle_music(file, folder / 'audio' / 'AMR')
    for file in MY_OTHER:
        handle_other(file, folder / 'MY_OTHER')
    for file in GZ_ARCHIVES:
        handle_archive(file, folder / 'archives' / 'GZ')
    for file in ZIP_ARCHIVES:
        handle_archive(file, folder / 'archives' / 'ZIP')
    for file in TAR_ARCHIVES:
        handle_archive(file, folder / 'archives' / 'TAR')
    for file in AVI_VIDEO:
        handle_video(file, folder / 'videos' / 'AVI')
    for file in MP4_VIDEO:
        handle_video(file, folder / 'videos' / 'MP4')
    for file in MOV_VIDEO:
        handle_video(file, folder / 'videos' / 'MOV')
    for file in MKV_VIDEO:
        handle_video(file, folder / 'videos' / 'MKV')
    for file in DOC_DOCUMENT:
        handle_document(file, folder / 'documents' / 'DOC')
    for file in DOCX_DOCUMENT:
        handle_document(file, folder / 'documents' / 'DOCX')
    for file in TXT_DOCUMENT:
        handle_document(file, folder / 'documents' / 'TXT')
    for file in PDF_DOCUMENT:
        handle_document(file, folder / 'documents' / 'PDF')
    for file in XLSX_DOCUMENT:
        handle_document(file, folder / 'documents' / 'XLSX')
    for file in PPTX_DOCUMENT:
        handle_document(file, folder / 'documents' / 'PPTX')


    # Виконуємо реверс списку для того щоб видалити всі папки
    for folder in FOLDERS[::-1]:
        handle_folder(folder)



def sort_all():
    if sys.argv[1]:
        folder_for_scan = Path(sys.argv[1])
        print(f'Start in folder {folder_for_scan.resolve()}')
        main(folder_for_scan.resolve())



# if __name__ == '__main__':
#
#
#     folder_for_scan = sys.argv[1]
#     print(f'Start in folder {folder_for_scan}')
#
#
#     scan(Path(folder_for_scan))
#     print(f'Images jpeg: {JPEG_IMAGES}')
#     print(f'Images jpg: {JPG_IMAGES}')
#     print(f'Images svg: {SVG_IMAGES}')
#     print(f'Audio mp3: {MP3_AUDIO}')
#     print(f'Archives zip: {ZIP_ARCHIVES}')
#     print(f'Video avi:{AVI_VIDEO}')
#     print(f'Video mp4:{MP4_VIDEO}')
#     print(f'Video mov:{MOV_VIDEO}')
#     print(f'Video mkv:{MKV_VIDEO}')
#     print(f'Document doc:{DOC_DOCUMENT}')
#     print(f'Document docx:{DOCX_DOCUMENT}')
#     print(f'Document txt:{TXT_DOCUMENT}')
#     print(f'Document pdf:{PDF_DOCUMENT}')
#     print(f'Document xlsx:{XLSX_DOCUMENT}')
#     print(f'Document pptx:{PPTX_DOCUMENT}')
#     print(f'Document mp3:{MP3_AUDIO}')
#     print(f'Audio ogg:{OGG_AUDIO}')
#     print(f'Audio wav:{WAV_AUDIO}')
#     print(f'Audio amr:{AMR_AUDIO}')
#     print(f'Archives gz:{GZ_ARCHIVES}')
#     print(f'Archives tar:{TAR_ARCHIVES}')
#
#
#     print(f'Types of files in folder: {EXTENSIONS}')
#     print(f'Unknown files of types: {UNKNOWN}')


    print(FOLDERS[::-1])


# запускаємо:  python3 main.py `назва_папки_для_сортування`